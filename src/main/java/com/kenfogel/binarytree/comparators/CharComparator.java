package com.kenfogel.binarytree.comparators;

import java.util.Comparator;

/**
 * compare 2 chars by their ascii values
 * @Author: Saad Khan
 */
public class CharComparator implements Comparator<Character> {
    @Override
    public int compare(Character o1, Character o2) {
        return Character.compare(o1, o2);
    }
}
