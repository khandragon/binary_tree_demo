package com.kenfogel.binarytree.comparators;

import java.util.Comparator;

/**
 * compare two Integers and return the larger of the two
 * @Author Saad Khan
 */
public class IntegerComparator implements Comparator<Integer> {
    @Override
    public int compare(Integer o1, Integer o2) {
        return o1.compareTo(o2);
    }
}
