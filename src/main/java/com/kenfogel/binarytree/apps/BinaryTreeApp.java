package com.kenfogel.binarytree.apps;

import com.kenfogel.binarytree.comparators.IntegerComparator;
import com.kenfogel.binarytree.implementation.BinaryTree;

/**
 * Based on the code found at http://cslibrary.stanford.edu/110/BinaryTrees.html
 *
 * @author Saad Khan
 */
public class BinaryTreeApp {

    private BinaryTree tree;

    /**
     * Run the various methods in a tree to test it.
     */
    public void perform() {
        IntegerComparator ic = new IntegerComparator();
        tree = new BinaryTree(ic);
        //int[] data = {4, 2, 8, 9, 1, 3, 7, 5, 6, 0};
        int[] data = {4, 2, 8, 9, 1, 3, 7, 5, 6, 0, 4, 7, 2, 435, 13};
        buildATree(data);
        System.out.println("Look up 7 = " + tree.lookup(7));
        System.out.println("Look up 10 = " + tree.lookup(10));
        System.out.println("Size of tree = " + tree.size());
        System.out.println("Max depth = " + tree.maxDepth());
        System.out.println("Min value = " + tree.minValue());
        System.out.print("Inorder = ");
        tree.printInorderTree();
        System.out.print("Postorder = ");
        tree.printPostorder();
        System.out.println("The Paths");
        tree.printPaths();
        System.out.println("The Tree");
        tree.printLineByLine();
    }

    /**
     * public getter for tree and its components
     * @return
     */
    public BinaryTree getTree() {
        return tree;
    }

    /**
     * Build a tree by inserting the members of the array into the tree.
     */
    public void buildATree(int data[]) {
        for (int number : data) {
            tree.insert(number);
        }
    }
}
