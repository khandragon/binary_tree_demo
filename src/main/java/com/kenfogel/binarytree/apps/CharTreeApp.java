package com.kenfogel.binarytree.apps;

import com.kenfogel.binarytree.comparators.CharComparator;
import com.kenfogel.binarytree.implementation.BinaryTree;

/**
 * Based on the code found at http://cslibrary.stanford.edu/110/BinaryTrees.html
 *
 * @author Saad Khan
 */
public class CharTreeApp {

    private BinaryTree tree;

    /**
     * Run the various methods in a tree to test it.
     */
    public void perform() {
        CharComparator cc = new CharComparator();
        tree = new BinaryTree(cc);
        char[] data = {'f', 'g', 'r', 'h', 'd', 'w', 'j', 'e', 'v', 'l'};
        buildATree(data);
        System.out.println("Look up 7 = " + tree.lookup('e'));
        System.out.println("Look up 10 = " + tree.lookup('f'));
        System.out.println("Size of tree = " + tree.size());
        System.out.println("Max depth = " + tree.maxDepth());
        System.out.println("Min value = " + tree.minValue());
        System.out.print("Inorder = ");
        tree.printInorderTree();
        System.out.print("Postorder = ");
        tree.printPostorder();
        System.out.println("The Paths");
        tree.printPaths();
        System.out.println("The Tree");
        tree.printLineByLine();
    }

    /**
     * Build a tree by inserting the members of the array into the tree.
     *
     * @param data
     */
    public void buildATree(char data[]) {
        for (char character : data) {
            tree.insert(character);
        }
    }

    /**
     * public getter for the tree components
     * @return
     */
    public BinaryTree getTree() {
        return tree;
    }

}
