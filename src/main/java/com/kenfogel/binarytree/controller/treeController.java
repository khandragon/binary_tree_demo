package com.kenfogel.binarytree.controller;

import com.kenfogel.binarytree.apps.BinaryTreeApp;
import com.kenfogel.binarytree.apps.CharTreeApp;
import com.kenfogel.binarytree.implementation.BinaryTree;
import com.kenfogel.binarytree.implementation.BinaryTreeNode;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

/**
 * Control display of trees
 *
 * @Author: Saad Khan
 */
public class treeController implements Initializable {


    @FXML // fx:id="mainScreen"
    private AnchorPane mainScreen; // Value injected by FXMLLoader

    private BinaryTree tree;
    private ArrayList<BinaryTreeNode> listNodes;

    /**
     * Called to initialize a controller after its root element has been
     * completely processed.
     *
     * @param location  The location used to resolve relative paths for the root object, or
     *                  <tt>null</tt> if the location is not known.
     * @param resources The resources used to localize the root object, or <tt>null</tt> if
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    /**
     * draws tree with proper space
     */
    public void drawTree() {
        double multiplier = tree.maxDepth();
        for (BinaryTreeNode node : listNodes) {
            if (node.isRoot()) {
                node.setxPosition(mainScreen.getWidth() / 2);
                node.setyPosition(25);
            }

            System.out.println(node.getData() + " : " + node.getxPosition() + " : " + node.getyPosition());
            drawCircle(node.getData().toString(), node.getxPosition(), node.getyPosition());
            //fix multiplier
            System.out.println("multiplier " + multiplier);

            if (node.getLeft() != null) {
                node.getLeft().setxPosition(node.getxPosition() - (5 * Math.pow(2, multiplier)));
                node.getLeft().setyPosition(node.getyPosition() + (2.5 * Math.pow(2, multiplier)));
                drawLine(node, node.getLeft());
            } else {
                multiplier -= 0.2;
            }
            if (node.getRight() != null) {
                node.getRight().setxPosition(node.getxPosition() + (5 * Math.pow(2, multiplier)));
                node.getRight().setyPosition(node.getyPosition() + (2.5 * Math.pow(2, multiplier)));
                drawLine(node, node.getRight());
            } else {
                multiplier -= 0.2;
            }
        }
    }

    /**
     * Draws a line from one parent to a child node
     */
    private void drawLine(BinaryTreeNode node, BinaryTreeNode child) {
        Line l = new Line(node.getxPosition(), node.getyPosition(), child.getxPosition(), child.getyPosition());
        mainScreen.getChildren().addAll(l);
    }

    /**
     * draws a circle using information from the node and the positions of where to place current
     */
    private void drawCircle(String data, double xPost, double yPost) {
        Text label = new Text(xPost, yPost, String.valueOf(data));
        label.setFill(Color.WHITE);
        label.setFont(Font.font("Comic Sans Ms", 20));
        Ellipse circle = new Ellipse(xPost, yPost, 50.0, 25.0);
        mainScreen.getChildren().addAll(circle, label);
    }

    /**
     * sets the type or which type of tree to be displayed and intanties the proper tree
     */
    public void setType(String type) {
        if (type.equals("Integer")) {
            BinaryTreeApp ba = new BinaryTreeApp();
            ba.perform();
            this.tree = ba.getTree();
        } else {
            CharTreeApp ca = new CharTreeApp();
            ca.perform();
            this.tree = ca.getTree();
        }
        this.listNodes = tree.getNodes();
    }
}
