package com.kenfogel.binarytree.controller;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Control tree display main menu
 * @Author: Saad Khan
 */
public class mainController {

    @FXML // fx:id="intergerBtn"
    private Button intergerBtn; // Value injected by FXMLLoader

    @FXML // fx:id="charBtn"
    private Button charBtn; // Value injected by FXMLLoader

    //type of btn clicked
    private String type;

    /**
     * When the integer btn is clicked set the type to integer and create the stage
     */
    public void openIntegerTree(ActionEvent actionEvent) throws IOException {
        type = "Integer";
        openTreeCreator();
    }

    /**
     * When the char btn is clicked set the type to char and create the stage
     */
    public void openCharTree(ActionEvent actionEvent) throws IOException {
        type = "Char";
        openTreeCreator();
    }

    /**
     * creates a stage of where the tree will be displayed
     */
    private void openTreeCreator() throws IOException {
        Stage primaryStage = (Stage) intergerBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/tree.fxml"));
        Parent root = (Parent) loader.load();
        treeController controller = (treeController) loader.getController();
        controller.setType(type);
        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/Styles.css");
        Stage treeDisplayStage = new Stage();
        treeDisplayStage.getIcons().add(new Image("file:icon.png"));
        treeDisplayStage.setTitle("BinaryTree App: Show");
        treeDisplayStage.initModality(Modality.APPLICATION_MODAL);
        treeDisplayStage.setScene(scene);
        treeDisplayStage.show();
        controller.drawTree();
    }
}
