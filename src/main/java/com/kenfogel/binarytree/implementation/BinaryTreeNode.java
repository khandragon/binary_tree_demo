package com.kenfogel.binarytree.implementation;

/**
 * Based on the code found at http://cslibrary.stanford.edu/110/BinaryTrees.html
 *
 * @author Ken Fogel
 */
public class BinaryTreeNode<T> {

    // package access to support reading and writing directly
    // from the BinaryTree class without the need for setters & getters
    BinaryTreeNode<T> left;
    BinaryTreeNode<T> right;
    T data;

    boolean LRBranch;
    boolean isRoot;
    double xPosition;
    double yPosition;

    /**
     * Constructor that creates nodes
     */
    public BinaryTreeNode(T newData) {
        left = null;
        right = null;
        data = newData;
    }

    public BinaryTreeNode<T> getLeft() {
        return left;
    }

    public BinaryTreeNode<T> setLeft(BinaryTreeNode<T> left) {
        this.left = left;
        return this;
    }

    public BinaryTreeNode<T> getRight() {
        return right;
    }

    public BinaryTreeNode<T> setRight(BinaryTreeNode<T> right) {
        this.right = right;
        return this;
    }

    public T getData() {
        return data;
    }

    public BinaryTreeNode<T> setData(T data) {
        this.data = data;
        return this;
    }

    public boolean isRoot() {
        return isRoot;
    }

    public BinaryTreeNode<T> setRoot(boolean root) {
        isRoot = root;
        return this;
    }

    public double getxPosition() {
        return xPosition;
    }

    public BinaryTreeNode<T> setxPosition(double xPosition) {
        this.xPosition = xPosition;
        return this;
    }

    public double getyPosition() {
        return yPosition;
    }

    public BinaryTreeNode<T> setyPosition(double yPosition) {
        this.yPosition = yPosition;
        return this;
    }

    public boolean isLRBranch() {
        return LRBranch;
    }

    public BinaryTreeNode<T> setLRBranch(boolean LRBranch) {
        this.LRBranch = LRBranch;
        return this;
    }

}
