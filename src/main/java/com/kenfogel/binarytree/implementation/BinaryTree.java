package com.kenfogel.binarytree.implementation;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Queue;

/**
 * Based on the code found at http://cslibrary.stanford.edu/110/BinaryTrees.html
 *
 * @author Saad Khan
 */
public class BinaryTree<T> {

    // Root node reference. Will be null for an empty tree.
    private BinaryTreeNode<T> root;
    private Comparator comp;
    private ArrayList<BinaryTreeNode> nodesList;

    /**
     * Creates an empty binary tree -- a null root reference.
     */
    public BinaryTree(Comparator comparator) {
        root = null;
        this.comp = comparator;
        this.nodesList = new ArrayList<>();
    }

    /**
     * Inserts the given data into the binary tree.Uses a recursive helper.
     */
    public void insert(T data) {
        root = insert(root, data);
    }

    private BinaryTreeNode<T> insert(BinaryTreeNode<T> node, T data) {
        if (node == null) {
            node = new BinaryTreeNode<>(data);
        } else {
            if (comp.compare(data, node.data) <= 0) {
                node.left = insert(node.left, data);
            } else {
                node.right = insert(node.right, data);
            }
        }
        return (node); // in any case, return the new reference to the caller
    }

    /**
     * Returns true if the given target is in the binary tree.Uses a recursive
     * helper.
     *
     * @return true of false depending on whether the data is found
     */
    public boolean lookup(T data) {
        return (lookup(root, data));
    }

    /**
     * Recursive lookup -- given a node, recur down searching for the given
     * data.
     */
    private boolean lookup(BinaryTreeNode<T> node, T data) {
        if (node == null) {
            return (false);
        }

        if (comp.compare(data, node.data) == 0) {
            return (true);
        } else if (comp.compare(data, node.data) < 0) {
            return (lookup(node.left, data));
        } else {
            return (lookup(node.right, data));
        }
    }

    ///////////////////////////////////////////////////////////////////////////

    /**
     * Returns the number of nodes in the tree. Uses a recursive helper that
     * recurses down the tree and counts the nodes.
     *
     * @return the number of elements in the tree
     */
    public int size() {
        return (size(root));
    }

    private int size(BinaryTreeNode node) {
        if (node == null) {
            return (0);
        } else {
            return (size(node.left) + 1 + size(node.right));
        }
    }

    /**
     * Returns the max root-to-leaf depth of the tree. Uses a recursive helper
     * that recurses down to find the max depth.
     *
     * @return The depth of the tree from the root to the lowest node
     */
    public int maxDepth() {
        return (maxDepth(root));
    }

    private int maxDepth(BinaryTreeNode node) {
        if (node == null) {
            return (0);
        } else {
            int lDepth = maxDepth(node.left);
            int rDepth = maxDepth(node.right);

            // use the larger + 1 
            return (Math.max(lDepth, rDepth) + 1);
        }
    }

    /**
     * Returns the min value in a non-empty binary search tree. Uses a helper
     * method that iterates to the left to find the min value.
     *
     * @return The smallest value in the tree
     */
    public T minValue() {
        return (minValue(root));
    }

    /**
     * Finds the min value in a non-empty binary search tree.
     */
    private T minValue(BinaryTreeNode node) {
        BinaryTreeNode current = node;
        while (current.left != null) {
            current = current.left;
        }

        return (T) current.data;
    }

    /**
     * Prints the node values in the "inorder" order. Uses a recursive helper to
     * do the traversal.
     */
    public void printInorderTree() {
        printInorderTree(root);
        System.out.println();
    }

    private void printInorderTree(BinaryTreeNode<T> node) {
        if (node == null) {
            return;
        }

        // left, node itself, right
        printInorderTree(node.left);
        System.out.print(node.data + "  ");
        printInorderTree(node.right);
    }

    /**
     * Prints the node values in the "postorder" order. Uses a recursive helper
     * to do the traversal.
     */
    public void printPostorder() {
        printPostorder(root);
        System.out.println();
    }

    private void printPostorder(BinaryTreeNode<T> node) {
        if (node == null) {
            return;
        }

        // first recur on both subtrees
        printPostorder(node.left);
        printPostorder(node.right);

        // then deal with the node
        System.out.print(node.data + "  ");
    }

    /**
     * Given a binary tree, prints out all of its root-to-leaf paths, one per
     * line. Uses a recursive helper to do the work.
     */
    public void printPaths() {
        ArrayList<T> path = new ArrayList<>();
        for (int i = 0; i < 1000; i++) {
            path.add(null);
        }
        printPaths(root, path, 0);
    }

    /**
     * Recursive printPaths helper -- given a node, and an array containing the
     * path from the root node up to but not including this node, prints out all
     * the root-leaf paths.
     */
    private void printPaths(BinaryTreeNode<T> node, ArrayList<T> path, int pathLen) {
        if (node == null) {
            return;
        }

        // append this node to the path array
        path.set(pathLen, node.data);
        pathLen++;

        // it's a leaf, so print the path that led to here
        if (node.left == null && node.right == null) {
            printArray(path, pathLen);
        } else {
            // otherwise try both subtrees
            printPaths(node.left, path, pathLen);
            printPaths(node.right, path, pathLen);
        }
    }

    /**
     * Utility that prints nodes from an array on one line.
     */
    private void printArray(ArrayList<T> nodes, int len) {
        int i;
        for (i = 0; i < len; i++) {
            System.out.print(nodes.get(i) + " ");
        }
        System.out.println();
    }

    /**
     * Prints out each level in the tree from left to right. Uses a recursive
     * helper to do the work.
     */
    public void printLineByLine() {
        printLineByLine(root);
    }

    private void printLineByLine(BinaryTreeNode<T> root) {
        if (root == null) {
            return;
        }
        final Queue<BinaryTreeNode> queue = new ArrayDeque<>();
        queue.add(root);
        root.setRoot(true);
        nodesList.add(root);
        while (!queue.isEmpty()) {
            final int size = queue.size();
            for (int i = 0; i < size; i++) {
                BinaryTreeNode<T> x = queue.remove();
                System.out.print(x.data + " ");
                if (x.left != null) {
                    queue.add(x.left);
                    x.left.setLRBranch(true);
                    x.left.setRoot(false);
                    nodesList.add(x.left);
                }
                if (x.right != null) {
                    queue.add(x.right);
                    x.right.setLRBranch(false);
                    x.right.setRoot(false);
                    nodesList.add(x.right);
                }
            }
            // new level
            System.out.println();
        }
    }

    public ArrayList<BinaryTreeNode> getNodes() {
        return nodesList;
    }
}